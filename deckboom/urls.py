
from django.conf.urls import url
from .rpc import rpcapp
from .views import (
    index, ConnectionCollection, ConnectionView, getqwc, support
)

urlpatterns = [
    # Spyne entry point
    url(r'^soap', rpcapp, name='soap'),

    # JSON APIs
    url(r'^connections/([^/]+)/qwc', getqwc),
    url(r'^connections/([^/]+)', ConnectionView.as_view()),
    url(r'^connections', ConnectionCollection.as_view()),

    # User-facing stuff
    url(r'^support', support, name='support'),
    url(r'^$', index, name='index'),
]
