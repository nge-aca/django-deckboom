from django.apps import AppConfig


class DeckboomConfig(AppConfig):
    name = 'deckboom'
