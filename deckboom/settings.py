# A callable, or dotted path to a callable that will get the appropriate
# queues/names for a user. Specifically, it takes a single ``User`` and should
# return a ``dict``-like, mapping URLs to display names.
DECKBOOM_QUEUES_PROVIDER = None

# Full URL of where users should go for help
DECKBOOM_SUPPORT_URL = None

# A version string used in constructing the version to
DECKBOOM_APP_VERSION = ""

# Additional fields for the QWC file
DECKBOOM_QWC_OPTIONS = {}

# Settings for the underlying boto library. See http://boto3.readthedocs.io/en/latest/reference/core/session.html#boto3.session.Session
DECKBOOM_AWS_SETTINGS = {}