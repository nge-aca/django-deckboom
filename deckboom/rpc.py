from spyne import ServiceBase, srpc, Application
from spyne.model import Unicode, Integer, Iterable
from spyne.protocol.soap.soap11 import Soap11
from spyne.server.django import DjangoApplication
from django.views.decorators.csrf import csrf_exempt
from contextlib import contextmanager
from importlib import import_module
from django.conf import settings
from django.contrib.sessions.backends.base import UpdateError
import logging

import boto3
try:
    import lxml.etree as etree
except ImportError:
    # Shouldn't happen because spyne's SOAP depends on lxml, but just in case
    import xml.etree.ElementTree as etree

# For converting message IDs
import hashlib
import string

from .models import Connection


boto3.setup_default_session(**settings.DECKBOOM_AWS_SETTINGS)

logger = logging.getLogger(__name__)


@contextmanager
def xmlsession(ticket):
    """
    Re-implements (read: steals from) SessionMiddleware
    """
    engine = import_module(settings.SESSION_ENGINE)
    session = engine.SessionStore(ticket)

    yield session

    try:
        modified = session.modified
    except AttributeError:
        pass
    else:
        # First check if we need to delete this cookie.
        # The session should be deleted only if the session is entirely empty
        if modified or settings.SESSION_SAVE_EVERY_REQUEST:
            try:
                session.save()
            except UpdateError:
                # Not quite sure what to do with this, other than NOT passing
                # it to QBWC. In HTTP, this means that the user was logged out.
                pass


@contextmanager
def newsession():
    """
    Note: session.id is not available until after the context manager
    """
    engine = import_module(settings.SESSION_ENGINE)
    session = engine.SessionStore()

    yield session

    # We can't really do much about duplicate keys, because the ticket is all
    # we have, and it must be the same as the company file name
    # (What's up with that???)
    session.create()


def deletesession(ticket):
    engine = import_module(settings.SESSION_ENGINE)
    session = engine.SessionStore(ticket)
    session.delete()


class QbwcService(ServiceBase):
    @srpc(Unicode, _returns=Unicode)
    def clientVersion(strVersion):
        # TODO: Figure out version compatibilities?
        return ""

    @srpc(Unicode, Unicode, _returns=Iterable(Unicode))
    def authenticate(strUserName, strPassword):
        # Note: The official documentation is grossly incorrect about this
        # method. See
        # http://wiki.consolibyte.com/wiki/doku.php/quickbooks_web_connector#array_authenticate_string_strusername_string_strpassword  # noqa
        try:
            conn = Connection.objects.get(id=strUserName)
        except (Connection.DoesNotExist, ValueError):
            return "", "nvu"
        if not conn.check_password(strPassword):
            return "", "nvu"
        # Company file name
        cofn = conn.companyFile
        # Session ticket
        with newsession() as session:
            session['connection'] = str(conn.id)
        return session.session_key, cofn or "", "", ""

    @srpc(Unicode, Unicode, Unicode, _returns=Unicode)
    def connectionError(ticket, hresult, message):
        # We can't really do anything about it other than log it
        # TODO: Log this against the Connection to be displayed to the user
        with xmlsession(ticket) as session:
            if 'connection' not in session:
                # Workflow horribly abandoned
                return "done"
            conn = Connection.objects.get(session['connection'])
            conn.error = message
            conn.save()
            if hresult in (...):
                # Clear the company file name, user needs to reselect it
                conn.companyFile = None
                conn.save()
                # I'm hoping that this will cause it to select the company
                # file, like with authenticate()
                return ""
            else:
                # This is a transient error; we just need to try again later
                return "done"

    @srpc(Unicode, Unicode, Unicode, Unicode, Integer, Integer,
          _returns=Unicode)
    def sendRequestXML(ticket, strHCPResponse, strCompanyFileName,
                       qbXMLCountry, qbXMLMajorVers, qbXMLMinorVers):
        with xmlsession(ticket) as session:
            logger.debug("Session %r", session)
            if 'connection' not in session:
                # Workflow horribly abandoned
                logger.warning("Session not initialized")
                return "NoOp"
            conn = Connection.objects.get(id=session['connection'])
            if conn.error:
                # It's not erroring; rejoice!
                conn.error = None
                conn.save()
            if strHCPResponse:
                session['HostCompanyPrefs'] = strHCPResponse
            if strCompanyFileName:
                conn.companyFile = strCompanyFileName
                conn.save()
            # Yes, having two saves is inefficient, but it should only happen
            # the first connection after the company file changes

            # I don't think this is used anywhere, but just in case...
            if 'lastError' in session:
                logger.debug("Error found in sendRequestXML: %r", session['lastError'])
                return ""

            # Actually get the message and send it on to QBWC
            sqs = boto3.resource('sqs')
            queue = sqs.Queue(conn.queue)
            for message in queue.receive_messages(MaxNumberOfMessages=1,
                                                  WaitTimeSeconds=20,
                                                  MessageAttributeNames=['All'],
                                                  AttributeNames=['All']):
                logger.info("Got message %r", message)
                logger.debug("Body: %r", message.body)
                logger.debug("Attrs: %r", message.attributes)
                logger.debug("MAttrs: %r", message.message_attributes)
                session['lastMessage'] = message.receipt_handle
                session['lastMessageAttributes'] = message.message_attributes
                rv = request2qbXML(message)
                logger.debug("sendRequestXML Output: %r", rv)
                return rv
            else:
                logger.debug("No messages available")
                return ""

    @srpc(Unicode, Unicode, Unicode, Unicode, _returns=Integer)
    def receiveResponseXML(ticket, response, hresult, message):
        assert not (response and hresult)
        with xmlsession(ticket) as session:
            if 'connection' not in session:
                # Workflow horribly abandoned
                logger.warning("Session not initialized")
                return 100
            logger.debug("receiveResponseXML: %r %r %r", response, hresult, message)
            if hresult == '0x80040400':  
                # "QuickBooks found an error when parsing the provided XML text stream."
                # This is a bug with us, because we reparse all incoming XML
                logger.error("QuickBooks reported XML error in %r", session.get('lastMessage', None))
                return 2
            respcode = body = None
            if response:
                respcode, body = response2qbXML(response)
            conn = Connection.objects.get(id=session['connection'])
            sqs = boto3.resource('sqs')
            msg = sqs.Message(conn.queue, session['lastMessage'])
            mattr = session['lastMessageAttributes']

            logger.debug("Attributes: %r", mattr)

            replyQueue = None
            if mattr:
                replyQueue = mattr.get('replyQueue', {}).get('StringValue')

            msg.delete()

            if replyQueue:
                attrs = mattr.copy()
                if hresult:
                    attrs['hresult'] = {
                        'StringValue': str(hresult),
                        'DataType': 'String'
                    }
                if message:
                    attrs['message'] = {
                        'StringValue': str(message),
                        'DataType': 'String'
                    }

                queue = sqs.Queue(replyQueue)
                queue.send_message(MessageBody=body, MessageAttributes=attrs)
            else:
                logger.warning("No reply queue given")

            return 1  # We'll never definitively know if there's no jobs
            # There's always a chance that SQS didn't ask the right servers
            # Also, we can't count the number of jobs queued, so it's
            # impossible to calculate a percent

    @srpc(Unicode, _returns=Unicode)
    def getLastError(ticket):
        with xmlsession(ticket) as session:
            logger.debug("getLastError -> %r", session.get('lastError', None))
            if 'lastError' not in session:
                return "NoOp"
            return session.pop('lastError')

    @srpc(_returns=Unicode)
    def serverVersion():
        return "DeckBoom/dev"

    @srpc(Unicode, _returns=Unicode)
    def closeConnection(ticket):
        # Ignore any state in the session; let SQS recycle the open message
        deletesession(ticket)
        return "OK"


# This is 62 'digits'. With 24 of them, we can store 142 bits.
ID_DIGITS = string.digits + string.ascii_lowercase + string.ascii_uppercase
def id2msgset(mid):
    """
    Deterministically turns a message ID into something that fits in 24 characters.
    """
    # Must be 24 characters
    m = hashlib.md5()
    m.update(mid.encode('utf-8'))
    num = int(m.hexdigest(), base=16)
    if num == 0:
        return ID_DIGITS[0]
    arr = []
    base = len(ID_DIGITS)
    while num:
        num, rem = divmod(num, base)
        arr.append(ID_DIGITS[rem])
    arr.reverse()
    assert len(arr) <= 24, "{} -> {:x} -> {} > 24".format(mid, num, ''.join(arr))
    return ''.join(arr)


def request2qbXML(message):
    """
    Turns a request message in to valid qbXML.

    Mostly by validating, massaging xmlns, and providing the appropriate
    wrapper.
    """
    reqs = list(
        etree.fromstring(
            "<msg>{}</msg>".format(message.body)
        )
    )

    reqdoc = etree.Element('QBXML')
    # FIXME: Support oldMessageSetID
    msgset = etree.SubElement(reqdoc, 'QBXMLMsgsRq', {
        # We're setting 'continueOnError' instead of 'stopOnError' on the logic
        # that QB may reorder requests
        'onError': 'continueOnError',  # Really wish we had 'rollbackOnError'
        'newMessageSetID': str(id2msgset(message.message_id)),
    })
    msgset.extend(reqs)

    head = """<?xml version="1.0" encoding="utf-8"?><?qbxml version="13.0"?>"""

    return head.encode('utf-8') + etree.tostring(reqdoc)


def response2qbXML(response):
    """
    Turns a qbXML response into a message body.

    Mostly by unwrapping.
    """
    doc = etree.fromstring(response)
    elems = b""
    statusCode = ""
    for resp in doc.getiterator('QBXMLMsgsRs'):
        # Should only be one
        statusCode = resp.attrib.get('messageSetStatusCode')
        for e in resp:
            elems += etree.tostring(e)
    return statusCode, elems.decode('utf-8')


application = Application(
    [QbwcService],
    tns='http://developer.intuit.com/',
    in_protocol=Soap11(validator='lxml'),
    out_protocol=Soap11()
)

rpcapp = csrf_exempt(DjangoApplication(application))
