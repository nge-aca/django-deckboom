import json

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.utils.decorators import method_decorator
from django.utils.module_loading import import_string
from django.views.decorators.csrf import requires_csrf_token
from django.views.generic.base import View

from .models import Connection

try:
    import lxml.etree as etree
except ImportError:
    # Shouldn't happen because spyne's SOAP depends on lxml, but just in case
    import xml.etree.ElementTree as etree


@login_required
@requires_csrf_token
def index(request):
    connections = Connection.objects.filter(user=request.user)
    qprovider = settings.DECKBOOM_QUEUES_PROVIDER
    if not callable(qprovider):
        qprovider = import_string(settings.DECKBOOM_QUEUES_PROVIDER)
    queues = qprovider(request.user)
    for conn in connections:
        queues.pop(conn.queue, None)
    return render(request, "deckboom/index.html", {
        'queues': list(sorted(queues.items(), key=lambda kv: kv[1])),
        'connections': connections,
    })


def getdata(request):
    if request.content_type == 'application/json':
        return json.loads(request.body.decode('utf-8'))  # Per JSON spec
    elif request.content_type in ('application/x-www-form-urlencoded'
                                  'multipart/form-data'):
        return request.POST


class ConnectionCollection(View):
    @method_decorator(login_required)
    def get(self, request):
        conns = Connection.objects.filter(user=request.user)
        return JsonResponse({
            'connections': [
                conn.__json__()
                for conn in conns
            ]
        })

    @method_decorator(login_required)
    def post(self, request):
        conn = Connection()
        conn.user = request.user
        data = getdata(request)
        for k,v in data.items():
            if k == 'password':
                conn.set_password(v)
            else:
                setattr(conn, k, v)
        conn.save()
        return JsonResponse({
            '$ok': 'Connection created',
            '$self': '/connections/{}'.format(conn.id),  # FIXME: Use reverse()
        })

class ConnectionView(View):
    @method_decorator(login_required)
    def get(self, request, connid):
        conn = get_object_or_404(Connection, id=connid)
        return JsonResponse(conn.__json__())

    @method_decorator(login_required)
    def put(self, request, connid):
        conn = get_object_or_404(Connection, id=connid)
        data = getdata(request)
        for k,v in data.items():
            if k == 'password':
                conn.set_password(v)
            else:
                setattr(conn, k, v)
        conn.save()
        return JsonResponse({
            '$ok': "Connection saved"    
        })

    @method_decorator(login_required)
    def delete(self, request, connid):
        conn = get_object_or_404(Connection, id=connid)
        conn.delete()
        return JsonResponse({
            '$ok': "Connection deleted"    
        })

def formatuuid(u):
    return '{%s}' % u

@login_required
def getqwc(request, connid):
    SubElement = etree.SubElement
    conn = get_object_or_404(Connection, id=connid)

    qwc = etree.Element('QBWCXML')

    SubElement(qwc, 'AppURL').text = request.build_absolute_uri(
        reverse('deckboom:soap')
    )

    SubElement(qwc, 'AppSupport').text = request.build_absolute_uri(
        reverse('deckboom:support')
    )
    
    SubElement(qwc, 'CertURL').text = request.build_absolute_uri('/')
    
    SubElement(qwc, 'FileID').text = formatuuid(conn.id)
    SubElement(qwc, 'UserName').text = str(conn.id)

    sched = SubElement(qwc, 'Scheduler')
    #SubElement(sched, 'RunEveryNSeconds').text = '5'  # This may require tweaking
    SubElement(sched, 'RunEveryNMinutes').text = '1'  # Real time is broken in our version of QBWC

    SubElement(qwc, 'Style').text = "Document"  # Can also be "DocWrapped" or "RPC", but it's not well specified what's expected

    opts = settings.DECKBOOM_QWC_OPTIONS

    # Do this differently so that it appears in server logs instead of client computers
    REQUIRED = {'AppDescription', 'AppID', 'AppName', 'OwnerID', 'QBType'}
    for tag in REQUIRED:
        SubElement(qwc, tag).text = str(opts[tag])

    for tag, val in opts.items():
        if tag in REQUIRED:
            continue
        SubElement(qwc, tag).text = str(val)

    resp = HttpResponse(etree.tostring(qwc), content_type='application/x-qbwc+xml')  # Made up this mimetype
    resp['Content-Disposition'] = 'attachment; filename={}.qwc'.format(conn.name)
    return resp



def support(request):
    return redirect(settings.DECKBOOM_SUPPORT_URL)
