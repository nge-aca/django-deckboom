from django.contrib import admin
from .models import Connection


@admin.register(Connection)
class ConnectionAdmin(admin.ModelAdmin):
    list_display = ('name', 'queue', 'user')
    readonly_fields = ('password',)  # Until I feel like implementing proper password handling
