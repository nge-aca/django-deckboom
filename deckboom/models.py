from django.db import models
from django.conf import settings
from django.contrib.auth.hashers import make_password, check_password
import uuid


class Connection(models.Model):
    # Doubles as FileID for the QWC file
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    queue = models.URLField(unique=True)
    name = models.CharField(max_length=128)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    password = models.CharField(max_length=128)
    companyFile = models.CharField(max_length=260, blank=True, null=True)
    # Windows max path length is 260
    error = models.CharField(max_length=1024, blank=True, null=True)

    # Password management shamelessly copied from django.contrib.auth
    def set_password(self, raw_password):
        self.password = make_password(raw_password)

    def check_password(self, raw_password):
        """
        Return a boolean of whether the raw_password was correct. Handles
        hashing formats behind the scenes.
        """
        def setter(raw_password):
            self.set_password(raw_password)
            # Password hash upgrades shouldn't be considered password changes.
            self.save(update_fields=["password"])
        return check_password(raw_password, self.password, setter)

    def __str__(self):
        return "{} ({})".format(self.name, self.queue)

    def __json__(self):
        return {
            '$self': '/connections/{}'.format(self.id),  # FIXME: User reverse()
            'id': self.id,
            'queue': self.queue,
            'name': self.name,
            'companyFile': self.companyFile,
            'error': self.error
        }
