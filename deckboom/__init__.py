# Based on https://passingcuriosity.com/2010/default-settings-for-django-applications/ # noqa
def inject_app_defaults(application):
    """Inject an application's default settings"""
    from importlib import import_module
    try:
        # Import our default settings
        app_settings = import_module('%s.settings' % application)
    except ImportError:
        # No settings, move on
        return
    else:
        from django.conf import settings, global_settings

        # Add the values from the application.settings module
        for name in dir(app_settings):
            if name.isupper():
                # Add the value to the default settings module
                setattr(global_settings, name, getattr(app_settings, name))

                # Add the value to the settings, if not already present
                if not hasattr(settings, name):
                    setattr(settings, name, getattr(app_settings, name))

inject_app_defaults(__name__)
