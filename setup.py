from setuptools import setup

setup(
    name='django-deckboom',
    version='0.1dev',
    packages=['deckboom'],
    license='LGPL',
    long_description=open('README.rst', encoding='utf-8').read(),
    install_requires=[
        'Django~=1.10.0',
        'spyne~=2.12.11',
        'boto3~=1.4.0',
        'lxml'
    ]
)
