========
DeckBoom
========
django-deckboom is a Django app to act as a gateway to the QuickBooks Web Connector. It's based on message queues (specifically Amazon SQS) to queue up requests and then it uses queues to dispatch responses.

Pragmatically, this is basically a proxy between QBWC and Amazon SQS.

There is some user-facing web pages involved. Minimal, but you do need to set up authentication.

**Note:** ``lxml`` is used. You may need to install the appropriate ``libxml`` packages or use ``env STATIC_DEPS=true pip ...`` instead.

URLs
----
Just include ``deckboom.urls`` in your ``urls.py``.

Templates
---------
``base.html`` is used and suggested for applying your template.

Configuration
-------------

* **``DECKBOOM_QUEUES_PROVIDER``**: A callable, or dotted path to a callable that will get the appropriate queues/names for a user. Specifically, it takes a single ``User`` and should return a ``dict``-like, mapping URLs to display names.
* **``DECKBOOM_SUPPORT_URL``**: A URL that users will be directed to for support
* **``DECKBOOM_APP_VERSION``**: A version string used in constructing the version to pass to QBWC
* **``DECKBOOM_QWC_OPTIONS``**: A dictionary of additional values to pass in the QWC file. The specifics are described below.
* **``DECKBOOM_AWS_SETTINGS``**: A dictionary of options to pass to the underlying boto3 library. See the `boto3 reference <http://boto3.readthedocs.io/en/latest/reference/core/session.html#boto3.session.Session>`_ for the definition of this.

QWC Options
+++++++++++

The QWC file has several metadata and application-specific fields that DeckBoom cannot compute. Please see the QBSDK for the full specification of available fields.

The fields you're required to configure are:

* **``AppDescription``**: This brief description of the application is displayed in the QB web connector (in the authorization dialog and in the main QBWC form under the application name. For best results we recommend a maximum description size of 80 characters.
* **``AppID``**: The AppID of the application, supplied in the call to OpenConnection. Currently QB and QB POS don’t do much with the AppID, so you can supply the tag without supplying any value for it. However, you can supply an AppID value here if you want.
* **``AppName``**: The name of the application visible to the user. This name is displayed in the QB web connector. It is also the name supplied in the SDK OpenConnection call to QuickBooks or QuickBooks POS.
* **``OwnerID``**: This is a GUID that represents your application or suite of applications, if your application needs to store private data in the company file for one reason or another. One of the most common uses is to check (via a QuickBooks SDK CompanyQuery request) whether you have communicated with this company file before, and possibly some data about that communication.
* **``QBType``**: Specify the value QBFS if your web service is designed for use with QuickBooks Financial software.
  Specify the value QBPOS if your web service is designed for use with QuickBooks Point-of-Sale (QBPOS).

App Dependencies
----------------
``django.contrib.auth`` is utilized. The usual rules about eg ``User`` subclassing apply.

=============
SQS Interface
=============
The primary API interface is through SQS. Exactly which queue requests are received on is based on the return value of ``DECKBOOM_QUEUES_PROVIDER`` and what the user has configured.

Briefly: Messages on Amazon SQS consist of an unstructured body and a key/value set of attributes.

On the request message, the body must be an XML document fragment, consisting of several qbXML requests. These are wrapped in a qbXML message set and forwarded to the QBWC.

The response from QBWC is sent to the queue specified in the ``replyQueue`` attribute of the request message. If there is one, the unwrapped qbXML message is given as the response message body. The response message attributes are copied from the request message, with the addition of two: the error-related values ``hresult`` and ``message`` are added. It's recommended that applications use message attributes to transfer context and state.

For a friendlier interface, see the ``BulletproofSlider`` library.

===============================
Concurrency and Configuring SQS
===============================
To avoid data loss, it's recommended that Amazon SQS be configured in AP (>=1) mode. However, this means that when there's partitioning, there's the possibility that an API call submitted once will be given to DeckBoom more than once. (No, what you're thinking right now to "fix" this will likely have unexpected behavior during a partition. Look up the CAP theorem.)

Therefore, steps need to be taken to make operations idempotent, eg generating IDs on the client. This will handle delivery hiccups in SQS, processing failures in QuickBooks, node failures along the chain, and other issues.

DeckBoom uses the SQS message ID to generate the QuickBooks MessageSetId. This way, if the same message is delievered to DeckBoom twice, QuickBooks will see the same MessageSet. (I don't know if QuickBooks will do anything about this, though.)

On the flip side, even if requests are idempotent, responses will still be returned more than once. Therefore, any code depending on data from QuickBooks must also be idempotent.

Other SQS options are up to the administrator, however, a message delay of 0 is suggested, and it's recommended to have a dead letter queue so that errors are detectable.
